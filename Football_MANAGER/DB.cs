﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Football_MANAGER
{
    public partial class DB : MetroForm
    {
        SQLiteConnection DBc = new SQLiteConnection("Data Source=fm.db3");

        public DB()
        {
            InitializeComponent();
        }

        private void DB_Load(object sender, EventArgs e)
        {

        }

        void load1 ()
        {
            DBc.Open();

            SQLiteDataAdapter da = new SQLiteDataAdapter("select * from Players", DBc);
            SQLiteCommandBuilder cd = new SQLiteCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];

            DBc.Close();
            dataGridView1.Columns[1].Width = 90;
            dataGridView1.Columns[2].Width = 90;
            dataGridView1.Columns[3].Width = 65;
            dataGridView1.Columns[4].Width = 80;
            dataGridView1.Columns[5].Width = 80;
            dataGridView1.Columns[6].Width = 60;
            dataGridView1.Columns[7].Width = 60;
            dataGridView1.Columns[8].Width = 60;
            dataGridView1.Columns[9].Width = 60;
        }
        void load2()
        {
            DBc.Open();

            SQLiteDataAdapter da = new SQLiteDataAdapter("select * from GoalKeepers ", DBc);
            SQLiteCommandBuilder cd = new SQLiteCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];

            DBc.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                load1();
            }
            if (comboBox1.SelectedIndex == 1)
            {
                load2();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    string message = "Вы уверены что хотите удалить игрока "+ dataGridView1[1, e.RowIndex].Value.ToString() + " "+ dataGridView1[2, e.RowIndex].Value.ToString() + " с датой рождения  "+ dataGridView1[3, e.RowIndex].Value.ToString() + " ?";
                    string caption = "Сообщение";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result;

                    result = MessageBox.Show(message, caption, buttons);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        DBc.Open();
                        SQLiteCommand cmd = DBc.CreateCommand();
                        cmd.CommandText = "delete from Players where FirstName= '"+dataGridView1[1,e.RowIndex].Value.ToString()+"' and LastName = '"+ dataGridView1[2, e.RowIndex].Value.ToString() + "'  and Age = '" + dataGridView1[3, e.RowIndex].Value.ToString() + "' ";
                        cmd.ExecuteNonQuery();
                        DBc.Close();
                    }
                    load1();
                }
                if (comboBox1.SelectedIndex == 1)
                {
                    string message = "Вы уверены что хотите удалить игрока " + dataGridView1[1, e.RowIndex].Value.ToString() + " " + dataGridView1[2, e.RowIndex].Value.ToString() + " с датой рождения " + dataGridView1[3, e.RowIndex].Value.ToString() + "?";
                    string caption = "Сообщение";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result;

                    result = MessageBox.Show(message, caption, buttons);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        DBc.Open();
                        SQLiteCommand cmd = DBc.CreateCommand();
                        cmd.CommandText = "delete from GoalKeepers where FirstName= '" + dataGridView1[1, e.RowIndex].Value.ToString() + "' and LastName = '" + dataGridView1[2, e.RowIndex].Value.ToString() + "'  and DateOfBirth = '" + dataGridView1[3, e.RowIndex].Value.ToString() + "'";
                        cmd.ExecuteNonQuery();
                        DBc.Close();
                    }
                    load2();
                }
            }
        }
    }
}
