﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace Football_MANAGER
{
    public partial class squad : MetroForm
    {
        int ct = 0;
        int co = 0;

        public squad()
        {
            InitializeComponent();
        }

        public void squad_Load(object sender, EventArgs e)
        {
            SQLiteConnection DB = new SQLiteConnection("Data Source=fm.db3");

            DB.Open();

            SQLiteDataAdapter da = new SQLiteDataAdapter("select * from Players", DB);

            SQLiteCommandBuilder cd = new SQLiteCommandBuilder(da);

            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];

            DB.Close();

            for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
            {
                var datestr1 = DateTime.Parse(dataGridView1[3, i].Value.ToString());
                var datestr2 = DateTime.Now;

                var timeSpan = (datestr1 - datestr2).Duration();
                var yearDiff = timeSpan.Days;

                dataGridView1[3, i].Value = yearDiff / 365;
            }

            DB.Open();

            SQLiteDataAdapter da1 = new SQLiteDataAdapter("select * from GoalKeepers ", DB);

            SQLiteCommandBuilder cd1 = new SQLiteCommandBuilder(da1);

            DataSet ds1 = new DataSet();
            da1.Fill(ds1);
            dataGridView4.DataSource = ds1.Tables[0];

            DB.Close();

            
               for (int i = 0; i <= dataGridView4.RowCount - 1; i++)
               {
                    var datestr1 = DateTime.Parse(dataGridView4[3, i].Value.ToString());
                    var datestr2 = DateTime.Now;

                    var timeSpan = (datestr1 - datestr2).Duration();
                    var yearDiff = timeSpan.Days;

                    dataGridView4[3, i].Value = yearDiff / 365;
               }
          
            

            dataGridView1.Columns[1].HeaderText = "Имя";
            dataGridView1.Columns[2].HeaderText = "Фамилия";
            dataGridView1.Columns[3].HeaderText = "Возраст";
            dataGridView1.Columns[4].HeaderText = "Рост,см";
            dataGridView1.Columns[5].HeaderText = "Ведущая нога";
            dataGridView1.Columns[6].HeaderText = "Скорость";
            dataGridView1.Columns[7].HeaderText = "Пасы";
            dataGridView1.Columns[8].HeaderText = "Удары";
            dataGridView1.Columns[9].HeaderText = "Защита";
            dataGridView1.Columns[1].Width = 60;
            dataGridView1.Columns[2].Width = 80;
            dataGridView1.Columns[3].Width = 65;
            dataGridView1.Columns[4].Width = 51;
            dataGridView1.Columns[5].Width = 78;
            dataGridView1.Columns[6].Width = 59;
            dataGridView1.Columns[7].Width = 47;
            dataGridView1.Columns[8].Width = 47;
            dataGridView1.Columns[9].Width = 48;

            dataGridView4.Columns[1].HeaderText = "Имя";
            dataGridView4.Columns[2].HeaderText = "Фамилия";
            dataGridView4.Columns[3].HeaderText = "Возраст,лет";
            dataGridView4.Columns[4].HeaderText = "Рост,см";
            dataGridView4.Columns[5].HeaderText = "Ведущая нога";
            dataGridView4.Columns[1].Width = 90;
            dataGridView4.Columns[2].Width = 90;
            dataGridView4.Columns[3].Width = 90;
            dataGridView4.Columns[4].Width = 90;
            dataGridView4.Columns[5].Width = 90;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (dataGridView1[1, e.RowIndex].Style.BackColor != Color.Red)
                {
                    if (e.ColumnIndex == 0)
                    {
                        if (dataGridView2.RowCount != 10)
                        {
                            if (comboBox1.SelectedIndex != -1)
                            {
                                if (dataGridView2.RowCount == ct)
                                {
                                    dataGridView2.RowCount++;
                                }
                                dataGridView2[e.ColumnIndex, ct].Value = dataGridView1[2, e.RowIndex].Value.ToString();


                                for (int i = 1; i <= 9; i++)
                                {
                                    dataGridView1[i, e.RowIndex].Style.BackColor = Color.Red;
                                    dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                                }
                                label4.Text = "Игроков в составе : " + dataGridView2.RowCount.ToString();
                                ct++;
                            }
                            else
                            {
                                MessageBox.Show("Сначала должена быть выбрана схема матча", "Ошибка!");
                            }

                            if (dataGridView2.RowCount == 10)
                            {
                                button2.Enabled = true;
                            }
                        }
                        else
                        {
                            MessageBox.Show("В основе не может быть больше 10 человек", "Ошибка!");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Игрок уже добавлен в состав", "Ошибка!");
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0: // схема 3-4-3
                    {

                        pictureBox1.Image = Properties.Resources._3_4_3;
                        dataGridView2.Columns.RemoveAt(1);
                        DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                        cmb.HeaderText = "Роль";
                        cmb.Name = "cmb";
                        cmb.Width = 265;
                        cmb.Items.Add("ЛЕВЫЙ ФЛАНГОВЫЙ АТАКУЮЩИЙ");
                        cmb.Items.Add("ЦЕНТР ФОРВАРД");
                        cmb.Items.Add("ПРАВЫЙ ФЛАНГОВЫЙ АТАКУЮЩИЙ");
                        cmb.Items.Add("ЛЕВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        dataGridView2.Columns.Add(cmb);
                        label1.Location = new Point(106, 202);
                        label2.Location = new Point(205, 169);
                        label3.Location = new Point(296, 202);
                        label5.Location = new Point(60, 306);
                        label6.Location = new Point(149, 333);
                        label7.Location = new Point(241, 333);
                        label8.Location = new Point(345, 306);
                        label9.Location = new Point(122, 456);
                        label10.Location = new Point(190, 456);
                        label11.Location = new Point(270, 456);
                        label12.Location = new Point(205, 548);
                        label1.Text = "Не задан";
                        label2.Text = "Не задан";
                        label3.Text = "Не задан";
                        label5.Text = "Не задан";
                        label6.Text = "Не задан";
                        label7.Text = "Не задан";
                        label8.Text = "Не задан";
                        label9.Text = "Не задан";
                        label10.Text = "Не задан";
                        label11.Text = "Не задан";
                        label12.Text = "Не задан";
                        break;
                    }
                case 1: // схема 3-5-2
                    {
                        pictureBox1.Image = Properties.Resources._3_5_2;
                        dataGridView2.Columns.RemoveAt(1);
                        DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                        cmb.HeaderText = "Роль";
                        cmb.Name = "cmb";
                        cmb.Width = 265;
                        cmb.Items.Add("ЛЕВЫЙ ФОРВАРД");
                        cmb.Items.Add("ПРАВЫЙ ФОРВАРД");
                        cmb.Items.Add("ЛЕВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        dataGridView2.Columns.Add(cmb);
                        label1.Location = new Point(167, 187);
                        label2.Location = new Point(241, 187);
                        label3.Location = new Point(57, 289);
                        label5.Location = new Point(205, 289);
                        label6.Location = new Point(351, 289);
                        label7.Location = new Point(134, 363);
                        label8.Location = new Point(272, 363);
                        label9.Location = new Point(125, 460);
                        label10.Location = new Point(196, 460);
                        label11.Location = new Point(272, 460);
                        label12.Location = new Point(205, 551);
                        label1.Text = "Не задан";
                        label2.Text = "Не задан";
                        label3.Text = "Не задан";
                        label5.Text = "Не задан";
                        label6.Text = "Не задан";
                        label7.Text = "Не задан";
                        label8.Text = "Не задан";
                        label9.Text = "Не задан";
                        label10.Text = "Не задан";
                        label11.Text = "Не задан";
                        label12.Text = "Не задан";
                        break;
                    }
                case 2: // схема 4-1-4-1
                    {
                        pictureBox1.Image = Properties.Resources._4_1_4_1;
                        dataGridView2.Columns.RemoveAt(1);
                        DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                        cmb.HeaderText = "Роль";
                        cmb.Name = "cmb";
                        cmb.Width = 265;
                        cmb.Items.Add("ЦЕНТР ФОРВАРД");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        dataGridView2.Columns.Add(cmb);
                        label1.Location = new Point(196, 177);
                        label2.Location = new Point(57, 311);
                        label3.Location = new Point(147, 311);
                        label5.Location = new Point(232, 311);
                        label6.Location = new Point(329, 311);
                        label7.Location = new Point(196, 379);
                        label8.Location = new Point(57, 460);
                        label9.Location = new Point(147, 460);
                        label10.Location = new Point(243, 460);
                        label11.Location = new Point(329, 460);
                        label12.Location = new Point(196, 547);
                        label1.Text = "Не задан";
                        label2.Text = "Не задан";
                        label3.Text = "Не задан";
                        label5.Text = "Не задан";
                        label6.Text = "Не задан";
                        label7.Text = "Не задан";
                        label8.Text = "Не задан";
                        label9.Text = "Не задан";
                        label10.Text = "Не задан";
                        label11.Text = "Не задан";
                        label12.Text = "Не задан";
                        break;
                    }
                case 3: // схема 4-2-3-1
                    {
                        pictureBox1.Image = Properties.Resources._4_2_3_1;//14154
                        dataGridView2.Columns.RemoveAt(1);
                        DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                        cmb.HeaderText = "Роль";
                        cmb.Name = "cmb";
                        cmb.Width = 265;
                        cmb.Items.Add("ЦЕНТР ФОРВАРД");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        dataGridView2.Columns.Add(cmb);
                        label1.Location = new Point(196, 176);
                        label2.Location = new Point(58, 270);
                        label3.Location = new Point(196, 270);
                        label5.Location = new Point(322, 270);
                        label6.Location = new Point(138, 372);
                        label7.Location = new Point(252, 372);
                        label8.Location = new Point(58, 460);
                        label9.Location = new Point(157, 460);
                        label10.Location = new Point(252, 460);
                        label11.Location = new Point(335, 460);
                        label12.Location = new Point(196, 547);
                        label1.Text = "Не задан";
                        label2.Text = "Не задан";
                        label3.Text = "Не задан";
                        label5.Text = "Не задан";
                        label6.Text = "Не задан";
                        label7.Text = "Не задан";
                        label8.Text = "Не задан";
                        label9.Text = "Не задан";
                        label10.Text = "Не задан";
                        label11.Text = "Не задан";
                        label12.Text = "Не задан";
                        break;
                    }
                case 4: // схема 4-4-2
                    {
                        pictureBox1.Image = Properties.Resources._4_4_2_flat;
                        dataGridView2.Columns.RemoveAt(1);
                        DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                        cmb.HeaderText = "Роль";
                        cmb.Name = "cmb";
                        cmb.Width = 265;
                        cmb.Items.Add("ПРАВЫЙ ФОРВАРД");
                        cmb.Items.Add("ЛЕВЫЙ ФОРВАРД");
                        cmb.Items.Add("ПРАВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        dataGridView2.Columns.Add(cmb);
                        label1.Location = new Point(157, 176);
                        label2.Location = new Point(228, 176);
                        label3.Location = new Point(58, 309);
                        label5.Location = new Point(145, 309);
                        label6.Location = new Point(243, 309);
                        label7.Location = new Point(335, 309);
                        label8.Location = new Point(58, 460);
                        label9.Location = new Point(157, 460);
                        label10.Location = new Point(252, 460);
                        label11.Location = new Point(335, 460);
                        label12.Location = new Point(196, 547);
                        label1.Text = "Не задан";
                        label2.Text = "Не задан";
                        label3.Text = "Не задан";
                        label5.Text = "Не задан";
                        label6.Text = "Не задан";
                        label7.Text = "Не задан";
                        label8.Text = "Не задан";
                        label9.Text = "Не задан";
                        label10.Text = "Не задан";
                        label11.Text = "Не задан";
                        label12.Text = "Не задан";
                        break;
                    }
                case 5: // схема 5-3-2
                    {
                        pictureBox1.Image = Properties.Resources._5_3_2;
                        dataGridView2.Columns.RemoveAt(1);
                        DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                        cmb.HeaderText = "Роль";
                        cmb.Name = "cmb";
                        cmb.Width = 265;
                        cmb.Items.Add("ПРАВЫЙ ФОРВАРД");
                        cmb.Items.Add("ЛЕВЫЙ ФОРВАРД");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ФЛАНГОВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ФЛАНГОВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        dataGridView2.Columns.Add(cmb);
                        label1.Location = new Point(157, 193);
                        label2.Location = new Point(230, 193);
                        label3.Location = new Point(130, 309);
                        label5.Location = new Point(270, 309);
                        label6.Location = new Point(196, 373);
                        label7.Location = new Point(39, 433);
                        label8.Location = new Point(97, 469);
                        label9.Location = new Point(196, 460);
                        label10.Location = new Point(288, 460);
                        label11.Location = new Point(347, 433);
                        label12.Location = new Point(196, 547);
                        label1.Text = "Не задан";
                        label2.Text = "Не задан";
                        label3.Text = "Не задан";
                        label5.Text = "Не задан";
                        label6.Text = "Не задан";
                        label7.Text = "Не задан";
                        label8.Text = "Не задан";
                        label9.Text = "Не задан";
                        label10.Text = "Не задан";
                        label11.Text = "Не задан";
                        label12.Text = "Не задан";
                        break;
                    }
                case 6: // схема 5-4-1
                    {
                        pictureBox1.Image = Properties.Resources._5_4_1;
                        dataGridView2.Columns.RemoveAt(1);
                        DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                        cmb.HeaderText = "Роль";
                        cmb.Name = "cmb";
                        cmb.Width = 265;
                        cmb.Items.Add("");
                        cmb.Items.Add("ЦЕНТР ФОРВАРД");
                        cmb.Items.Add("ПРАВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ПОЛУЗАЩИТНИК");
                        cmb.Items.Add("ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ЛЕВЫЙ ЗАЩИТНИК");
                        cmb.Items.Add("ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК");
                        dataGridView2.Columns.Add(cmb);
                        label1.Location = new Point(196, 220);
                        label2.Location = new Point(57, 367);
                        label3.Location = new Point(151, 373);
                        label5.Location = new Point(246, 373);
                        label6.Location = new Point(335, 367);
                        label7.Location = new Point(48, 460);
                        label8.Location = new Point(116, 474);
                        label9.Location = new Point(196, 460);
                        label10.Location = new Point(277, 474);
                        label11.Location = new Point(350, 460);
                        label12.Location = new Point(196, 547);
                        label1.Text = "Не задан";
                        label2.Text = "Не задан";
                        label3.Text = "Не задан";
                        label5.Text = "Не задан";
                        label6.Text = "Не задан";
                        label7.Text = "Не задан";
                        label8.Text = "Не задан";
                        label9.Text = "Не задан";
                        label10.Text = "Не задан";
                        label11.Text = "Не задан";
                        label12.Text = "Не задан";
                        break;
                    }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int s = 0;

            if (dataGridView2.RowCount == 10)
            {
                try
                {
                    for (int i = 0; dataGridView2.RowCount - 1 >= i; i++)
                    {
                        s = 0;
                        for (int j = 0; dataGridView2.RowCount - 1 >= j; j++)
                        {
                            if (dataGridView2[1, i].Value.ToString() == dataGridView2[1, j].Value.ToString())
                            {
                                s++;
                                if (s > 1)
                                {
                                    MessageBox.Show("Два или более игроков не могут быть назначены на одну позицию. Исправте ошибку в назначении роли и нажмите еще раз на 'Распределение'", "Ошибка!");
                                    return;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Должы быть выбраны роли всех игроков", "Ошибка!");
                }

                switch (comboBox1.SelectedIndex)
                {
                    case 0://выбор схемы 3-4-3
                        {
                            for (int i = 0; i <= dataGridView2.RowCount - 1; i++)//проверка всей таблицы
                            {
                                switch (dataGridView2[1, i].Value)//присваивание знаечения в схему
                                {
                                    case "ЛЕВЫЙ ФЛАНГОВЫЙ АТАКУЮЩИЙ":
                                        {
                                            label1.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТР ФОРВАРД":
                                        {
                                            label2.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ФЛАНГОВЫЙ АТАКУЮЩИЙ":
                                        {
                                            label3.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label5.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label6.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label7.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label8.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label9.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label10.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label11.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    case 1://выбор схемы 3-5-2
                        {
                            for (int i = 0; i <= dataGridView2.RowCount - 1; i++)//проверка всей таблицы
                            {
                                switch (dataGridView2[1, i].Value)//присваивание знаечения в схему
                                {
                                    case "ЛЕВЫЙ ФОРВАРД":
                                        {
                                            label1.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ФОРВАРД":
                                        {
                                            label2.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label3.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label5.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label6.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label7.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label8.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label9.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label10.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label11.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    case 2://выбор схемы 4-1-4-1
                        {
                            for (int i = 0; i <= dataGridView2.RowCount - 1; i++)//проверка всей таблицы
                            {
                                switch (dataGridView2[1, i].Value)//присваивание знаечения в схему
                                {
                                    case "ЦЕНТР ФОРВАРД":
                                        {
                                            label1.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label2.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label3.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label5.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label6.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label7.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЗАЩИТНИК":
                                        {
                                            label8.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label9.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label10.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЗАЩИТНИК":
                                        {
                                            label11.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    case 3://выбор схемы 4-2-3-1
                        {
                            for (int i = 0; i <= dataGridView2.RowCount - 1; i++)//проверка всей таблицы
                            {
                                switch (dataGridView2[1, i].Value)//присваивание знаечения в схему
                                {
                                    case "ЦЕНТР ФОРВАРД":
                                        {
                                            label1.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label2.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label3.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label5.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label6.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label7.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЗАЩИТНИК":
                                        {
                                            label8.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label9.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label10.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЗАЩИТНИК":
                                        {
                                            label11.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    case 4://выбор схемы 4-4-2
                        {
                            for (int i = 0; i <= dataGridView2.RowCount - 1; i++)//проверка всей таблицы
                            {
                                switch (dataGridView2[1, i].Value)//присваивание знаечения в схему
                                {
                                    case "ЛЕВЫЙ ФОРВАРД":
                                        {
                                            label1.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ФОРВАРД":
                                        {
                                            label2.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label3.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label5.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label6.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label7.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЗАЩИТНИК":
                                        {
                                            label8.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label9.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label10.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЗАЩИТНИК":
                                        {
                                            label11.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    case 5://выбор схемы 5-3-2
                        {
                            for (int i = 0; i <= dataGridView2.RowCount - 1; i++)//проверка всей таблицы
                            {
                                switch (dataGridView2[1, i].Value)//присваивание знаечения в схему
                                {
                                    case "ЛЕВЫЙ ФОРВАРД":
                                        {
                                            label1.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ФОРВАРД":
                                        {
                                            label2.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label3.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label5.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label6.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ФЛАНГОВЫЙ ЗАЩИТНИК":
                                        {
                                            label7.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label8.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label9.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label10.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ФЛАНГОВЫЙ ЗАЩИТНИК":
                                        {
                                            label11.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    case 6://выбор схемы 5-4-1
                        {
                            for (int i = 0; i <= dataGridView2.RowCount - 1; i++)//проверка всей таблицы
                            {
                                switch (dataGridView2[1, i].Value)//присваивание знаечения в схему
                                {
                                    case "ЦЕНТР ФОРВАРД":
                                        {
                                            label1.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label2.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label3.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ОПОРНЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label5.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ПОЛУЗАЩИТНИК":
                                        {
                                            label6.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЗАЩИТНИК":
                                        {
                                            label7.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЛЕВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label8.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label9.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЦЕНТРАЛЬНЫЙ ЗАЩИТНИК":
                                        {
                                            label10.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                    case "ПРАВЫЙ ЗАЩИТНИК":
                                        {
                                            label11.Text = dataGridView2[0, i].Value.ToString();
                                            break;
                                        }
                                }
                            }
                            break;
                        }
                }

                label12.Text = dataGridView3[0, 0].Value.ToString();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            dataGridView4.Visible = true;
        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 0 && co == 0)
                {
                    string message = "Вы уверены что хотите добавить этого вратаря в основу?";
                    string caption = "Сообщение";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result;

                    result = MessageBox.Show(message, caption, buttons);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (dataGridView3.RowCount == co)
                        {
                            dataGridView3.RowCount++;
                        }

                        dataGridView3[e.ColumnIndex, co].Value = dataGridView4[2, e.RowIndex].Value.ToString();
                        dataGridView3[e.ColumnIndex + 1, co].Value = "Вратарь";
                        co++;
                        label13.Text = "+ 1 вратарь";
                    }
                }
                else
                {
                    MessageBox.Show("Больше одного вратаря не может быть в основе", "Ошибка!");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ct = 0;
            co = 0;
            dataGridView2.DataSource = null;
            dataGridView2.Rows.Clear();
            dataGridView2.RowCount = 0;
            dataGridView3.DataSource = null;
            dataGridView3.Rows.Clear();
            dataGridView3.RowCount = 0;

            for (int i = 1; i <= dataGridView1.RowCount; i++)
            {
                for (int j = 2; j <= dataGridView1.ColumnCount; j++)
                {
                    dataGridView1[j - 1, i - 1].Style.BackColor = Color.White;
                }

            }
            dataGridView1.Visible = true;
            dataGridView4.Visible = false;
            label4.Text = "Игроков в составе : 0";
            label13.Text = "";
            button2.Enabled = false;
            label1.Text = "Не задан";
            label2.Text = "Не задан";
            label3.Text = "Не задан";
            label5.Text = "Не задан";
            label6.Text = "Не задан";
            label7.Text = "Не задан";
            label8.Text = "Не задан";
            label9.Text = "Не задан";
            label10.Text = "Не задан";
            label11.Text = "Не задан";
            label12.Text = "Не задан";
        }

            Bitmap bmp;
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
                e.Graphics.DrawImage(bmp, 0, 0);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView2.RowCount == 10 & dataGridView3.RowCount == 1)
            {
                Graphics g = this.CreateGraphics();
                bmp = new Bitmap(818, 660, g);
                Graphics mg = Graphics.FromImage(bmp);
                mg.CopyFromScreen(this.Location.X + 10, this.Location.Y + 12, 0, 0, this.Size);
                printPreviewDialog1.ShowDialog();
            }
            else
            {
                MessageBox.Show("Для печать должен быть выбран состав игроков");
            }
        }
    }     
}


