﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace Football_MANAGER
{
    public partial class wel : MetroForm
    {
        public wel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            addPL f1 = new addPL();
            f1.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            squad f1 = new squad();
            f1.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            addGK f1 = new addGK();
            f1.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DB f1 = new DB();
            f1.ShowDialog();
        }
    }
}
