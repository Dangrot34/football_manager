﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Football_MANAGER
{
    public partial class addGK : MetroForm
    {
        public addGK()
        {
            InitializeComponent();
        }

        private void addGK_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != "" & textBox12.Text != ""  & textBox2.Text != "" & Int32.Parse(textBox2.Text) < 250 & comboBox1.SelectedIndex != -1)
                {
                    SQLiteConnection DB = new SQLiteConnection("Data Source=fm.db3");
                    DB.Open();
                    SQLiteCommand cmd = DB.CreateCommand();
                    cmd.CommandText = "insert into GoalKeepers(FirstName,LastName,DateOfBirth,Height,Leg) values ('" + textBox1.Text + "','" + textBox12.Text + "', '" + dateTimePicker1.Value.ToShortDateString() + "','" + textBox2.Text + "','" + comboBox1.SelectedItem + "')";
                    cmd.ExecuteNonQuery();
                    DB.Close();

                    MessageBox.Show("Игрок добавлен в базу", "Добавлено!");
                    textBox1.Text = "";
                    textBox12.Text = "";
                    textBox2.Text = "";
                    comboBox1.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Проверьте правильность значений и повторите попытку");
                }
            }
            catch
            {
                MessageBox.Show("Проверьте правильность значений и повторите попытку");
            }
           
            
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }
    }
}
