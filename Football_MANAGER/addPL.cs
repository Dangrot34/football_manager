﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace Football_MANAGER
{
    public partial class addPL : MetroForm
    {
        public addPL()
        {
            InitializeComponent();
        }
        
        private void button2_Click(object sender, EventArgs e) // оценка
        {
            try
            {
                if (maskedTextBox2.MaskCompleted & textBox3.Text != "" & textBox4.Text != "" & textBox5.Text != "" & textBox6.Text != "" & textBox7.Text != "" & textBox8.Text != "" & textBox9.Text != "" & textBox10.Text != "" & textBox11.Text != "")
                {
                    double speed=0, sp = Double.Parse(maskedTextBox2.Text);
                    if (sp > 20)
                    {
                        label6.Text = "1";
                    }
                    else
                    {
                        speed = Math.Round((20 - sp) * 10, 0);

                        if (speed > 100)
                        {
                            label6.Text = "99";
                        }
                        else
                        {
                            label6.Text = speed.ToString();
                        }
                    }

                    label12.Text = ((Int32.Parse(textBox3.Text) + Int32.Parse(textBox4.Text) + Int32.Parse(textBox5.Text)) / 3).ToString();
                    label14.Text = ((Int32.Parse(textBox6.Text) + Int32.Parse(textBox7.Text) + Int32.Parse(textBox8.Text)) / 3).ToString();
                    label19.Text = ((Int32.Parse(textBox9.Text) + Int32.Parse(textBox10.Text) + Int32.Parse(textBox11.Text)) / 3).ToString();

                    if (textBox1.Text != ""& textBox12.Text != "" &  textBox2.Text != "" & Int32.Parse(textBox2.Text) < 250  & comboBox1.SelectedIndex != -1)
                    {
                        button3.Enabled = true;
                        MessageBox.Show("Теперь можно добавить игрока в базу данных");
                    }
                    else
                    {
                        MessageBox.Show("Заполните личные данные полностью");
                    }
                }
                else
                {
                    MessageBox.Show("Все поля должны быть заполнены");
                }
            }
            catch
            {
                MessageBox.Show("Проверьте правильность значений и повторите попытку");
            }
            
        }

        private void pictureBox5_MouseMove(object sender, MouseEventArgs e)//
        {
            pictureBox5.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Укажите в секундах с тысячной \nточностью за сколько пробежал \nигрок 100 метровку";
        }

        private void pictureBox5_MouseLeave(object sender, EventArgs e)
        {
            pictureBox5.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox2.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных пасов по воздуху \nсвоему партнеру по команде или в определенную \nзону из 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            pictureBox2.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox3.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных пасов на \nдальнее расстояние партнеру по команде \nиз 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            pictureBox3.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox4_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox4.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных пасов на \nближнее расстояние партнеру по команде \nиз 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox4_MouseLeave(object sender, EventArgs e)
        {
            pictureBox4.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox8_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox8.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных ударов по мячу на \nрасстояние 16+ метров \nиз 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox8_MouseLeave(object sender, EventArgs e)
        {
            pictureBox8.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox7_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox7.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных ударов \nпо мячу находящемуся в воздухе \nиз 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox7_MouseLeave(object sender, EventArgs e)
        {
            pictureBox7.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox6_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox6.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных пенальти из 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox6_MouseLeave(object sender, EventArgs e)
        {
            pictureBox6.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox11_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox11.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных подкатов из 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox11_MouseLeave(object sender, EventArgs e)
        {
            pictureBox11.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox10_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox10.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных отборов мяча \nу противника владеющего мячом из 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox10_MouseLeave(object sender, EventArgs e)
        {
            pictureBox10.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void pictureBox9_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox9.Image = Properties.Resources.infos;
            groupBox6.Visible = true;
            label23.Text = "Количество удачных перехватов мяча во время \nпаса противника из 100 попыток \n(совершать это можно не за одну тренировку)";
        }

        private void pictureBox9_MouseLeave(object sender, EventArgs e)
        {
            pictureBox9.Image = Properties.Resources.info;
            groupBox6.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                SQLiteConnection DB = new SQLiteConnection("Data Source=fm.db3");
                DB.Open();
                SQLiteCommand cmd = DB.CreateCommand();
                cmd.CommandText = "insert into Players(FirstName,LastName,Age,Height,Leg,Speed,Pass,Kick,Defense) values ('" +textBox1.Text + "','"+textBox12.Text+"', '" + dateTimePicker1.Value.ToShortDateString() + "','" + textBox2.Text + "','" + comboBox1.SelectedItem + "','" + label6.Text + "','" + label12.Text + "','" + label14.Text + "','" + label19.Text + "')";
                cmd.ExecuteNonQuery();
                DB.Close();

                MessageBox.Show("Игрок добавлен в базу", "Добавлено!");
                textBox1.Text = "";
                textBox12.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
                textBox9.Text = "";
                textBox10.Text = "";
                textBox11.Text = "";
                maskedTextBox2.Text = "";
                comboBox1.SelectedIndex = -1;
                label6.Text = "0";
                label12.Text = "0";
                label14.Text = "0";
                label19.Text = "0";
                button3.Enabled = false;
            }
            catch
            {
                MessageBox.Show("Проверьте правильность значений и повторите попытку");
            }

            
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8) 
            {
                e.Handled = true;
            }
        }

        private void addPL_Load(object sender, EventArgs e)
        {

        }
    }
}
